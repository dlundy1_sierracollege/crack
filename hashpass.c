#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "md5.h" //char *md5(const char *str, int length);

int main(int argc, char *argv[])
{
    FILE *out;
    FILE *in;
    
    if(argc != 3)
    {
        printf("Usage: %s word_to_hash\n", argv[0]);
        exit(1);
    }
    
    if(argc == 3)
    {
        in = fopen(argv[1], "r");
        out = fopen(argv[2], "w");
        
        if(!out)
        {
            printf("Can't open %s for writing.\n", argv[2]);
            exit(2);
        }
    }
    
    char line[100];
    
    while(fgets(line, 100, in) != NULL)
    {
        for(int i = 0; i < strlen(line); i++)
        {
            if(line[i] == '\n')
            {
                line[i] = '\0';
            }
        }
        
        char *hash = md5(line, strlen(line));
        fprintf(out, "%s\n", hash);
    }
    
    fclose(in);
    fclose(out);
}



